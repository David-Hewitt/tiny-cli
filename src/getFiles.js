const mime = require('mime-types');
const path = require('path');
const _ = require('lodash');
const fs = require('fs');

var mimeTypes = ['image/jpeg', 'image/png'];

const buildDir = directory => ({directory, files: []});

module.exports = function allFilesSync(dir, fileList = [buildDir(dir)]) {

	fs.readdirSync(dir).forEach(file => {

    	const filePath = path.resolve(dir, file)
    	const stat = fs.statSync(filePath)

    	if (stat && stat.isFile()) {
    		if (_.includes(mimeTypes, mime.lookup(filePath))) {
    			fileList.search('directory', dir).files.push(filePath);
    		}
    	} else {
    		fileList.push(buildDir(filePath));
    		allFilesSync(filePath, fileList);
    	}

  	})
  	return fileList
}