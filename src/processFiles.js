var casper = require('casper').create();

var files = JSON.parse(casper.cli.args[0]);
var authUrl = casper.cli.args[1];
casper.options.waitTimeout = 20000;

casper.userAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X)');

casper.start(authUrl);

casper.then(function () {
	this.page.uploadFile('.target [type="file"]', files);
});

casper.waitForSelector('.buttons .download', function () {
    
});

casper.waitFor(function check() {
    return this.evaluate(function() {
        return document.querySelectorAll('.buttons .download')[0].disabled == false;
    });
}, function then() {
    //this.echo('finished download');
}, function timeout() { // step to execute if check has failed
    this.echo('opps timeout :(').exit();
});

casper.thenClick('.buttons .download', function () {
    //this.echo('I clicked the button');

    //this.capture('testing.png');
});

casper.then(function () {
	var urls = this.evaluate(function() {
		var files = document.querySelectorAll('.files .upload');
		var download_urls = [];
		for (var i = 0; i < files.length; i++) {
			var file = files[i];
			download_urls.push(file.querySelectorAll('.after a')[0].href);
		}
		return download_urls;
	});

	console.log(JSON.stringify(urls));
});

casper.run(function() {
    this.exit();
});

casper.run();
