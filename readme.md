# Tiny PNG CLI

Tiny png cli is a tool designed to run a directory through [tiny png](https://tinypng.com) and *replace* the files with their compressed counteparts

```
$ tiny -url [auth url]
```