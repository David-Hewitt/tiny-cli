#!/usr/bin/env node

Array.prototype.search = require('./src/polyfills').search;
const packageVersion = require('./package.json').version;
const shell = require('child_process').execFile;
const getFiles = require('./src/getFiles');
const program = require('commander');
const chalk = require('chalk');
const https = require('https');
const path = require('path');
const _ = require('lodash');
const fs = require('fs');
const log = console.log;

program
	.version(packageVersion)
	.arguments('<url>', 'Provide the authentication url.')
	.parse(process.argv);

var authUrl = program.args[0] || '';

if (authUrl == '' || authUrl.indexOf('https://tinypng.com/#token') == -1) {
	log(chalk.red('Please provide tiny with an authentication url'));
	process.exit(1);
}

var cwd = process.cwd();

var files = getFiles(cwd);

(function filesLoop(i = 0) {

	var file = files[i];

	if (typeof file != 'undefined') {

		if (file.files.length == 0) {
			return filesLoop(++i);
		}

		var dir = files[i].directory;

		log(chalk.blue('currently in this directory: ' + dir));

		var chunks = _.chunk(file['files'], 20);

		(function chunksLoop(d) {

			var chunk = chunks[d];

			if (typeof chunk != 'undefined') {

				console.log('currently working on chunk: ', (d + 1));

				shell('casperjs', [path.join(__dirname, 'src', 'processFiles.js'), JSON.stringify(chunk), authUrl], function (err, stdout, stderr) {
					if (err) {
						console.log(err);
					}

					try {
						var urls = JSON.parse(stdout);
					} catch (e) {
						console.log(e);
					}

					(function urlLoop(e) {
						var url = urls[e];

						if (typeof url == 'string') {

							console.log('currently compressing: ', path.basename(url));

					
							var fullPath = path.join(dir, path.basename(url));

							var file = fs.createWriteStream(fullPath);
							var request = https.get(url, function(response) {
							  	response.pipe(file);

							  	if (e < urls.length) {
						  			urlLoop(++e);
								}

							  if (urls.length == e && d < chunks.length) {
							  	chunksLoop(++d);
							  }

							  if (urls.length == e && chunks.length == d && i < files.length) {
							  	// chunks are done
							  	filesLoop(++i);
							  }
							});
						}
					})(0)

				});
			}
		})(0);

	}

})(0);